const axios = require('axios');
const cron = require('node-cron');

const navItemsEndPoint = "";
const ordaNavIntegrationEndPoint = "";

// fetch data from the microsoft nav API
async function fetchData() {
    try {
        const response = await axios.get(`${navItemsEndPoint}`);
        return response.data;
    } catch (error) {
        console.error('Error fetching data:', error);
        return null;
    }
}

// send data to the orda API
async function sendData(data) {
    try {
        await axios.post(`${ordaNavIntegrationEndPoint}`, data);
        console.log('Data sent successfully');
    } catch (error) {
        console.error('Error sending data:', error);
    }
}

// Scheduled to run every minute
cron.schedule('* * * * *', async () => {
    console.log('Fetching data...');
    const data = await fetchData();
    if (data) {
        console.log('Sending data...');
        await sendData(data);
    }
});

console.log('Service started. Running tasks every minute.');
